# Generating a contract

## Prerequisites

Install Jinja:

```
pip install j2cli
```

Install asciidoc:
(Ubuntu steps below - see Asciidoc site for other platforms)

```
sudo apt-get install asciidoctor
sudo gem install asciidoctor-pdf --pre
```

## Building
To generate a contract, first create a file called `inputs.json`.  A sample is provided in `inputs.sample.json`.  

Note that `inputs.json` and the generated output folder are set to be ignored, to prevent accidentally committing them.

```
mkdir generated
j2 -f json freelancer-contract.adoc inputs.json > generated/freelancer-contract.adoc
asciidoctor-pdf generated/freelancer-contract.adoc
```
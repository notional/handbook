# Company Handbook

This repository contains documents and content about working at Notional.

It's pretty empty at the moment, because we're still getting going.

However, over time, it'll grow to contain poliices, documents, and how to's about
getting on at Notional.

Contributions are welcome - please raise a ticket if you have a question, or
raise a PR for changes that you think we should make.